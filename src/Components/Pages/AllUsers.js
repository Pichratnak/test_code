// import React from 'react'
import axios from 'axios'
import React,{useState, useEffect} from 'react'
 import PhotoCard from '../PhotoCard';
 
// import ItemCard from '../ItemCard'

export const AllUsers = () => {
    const [users, setUsers] = useState([])
    // write code to get data from api 
    useEffect(()=>{
        axios.get("https://api.escuelajs.co/api/v1/users")
        .then((response)=>setUsers(response.data))
        .catch((error)=> console.log("Error is : "  , error))
    },[])

    console.log("Users : ", users)
  return (
    <div className='container'>
        <div className="row">
            {users.map((user)=>(
              <div className="col-4">
                <PhotoCard user={user}/>
              </div>  
            ))}
        </div>
        


     </div>
  );
};
export default AllUsers;
