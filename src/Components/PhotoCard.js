import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';

function PhotoCard(props) {
  
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src= {props.item.url} />
      <Card.Body>
        <Card.Title> {props.item.title} </Card.Title>
        <Card.Text>
            {/* {props.item.description} */}
        </Card.Text>
        <Button variant="warning"  >Transform</Button>
      </Card.Body>
    </Card>
  );
}

export default PhotoCard;
