import axios from 'axios';
import React,{useState, useEffect} from 'react'
import PhotoCard from './PhotoCard';

export const AllPhotos = () => {
    const[photos,setPhotos] = useState([])
    useEffect (() => {
        axios.get("https://jsonplaceholder.typicode.com/photos")
        .then((respone)=> setPhotos(respone.data))
        .catch((err)=> console.error("Error :",err));
    },[]);
    
    console.log("Response back is:",photos)
  return (   
  <div className='container'>
  <div className="row">
  {
      photos.map((item)=>
      <div className='col-4'>
          <PhotoCard item = {item}/>

      </div>
      )
  }
  </div>
</div>
)
}

