import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as BigRouter, Routes, Route } from "react-router-dom";
import MyNavbar from "./Components/MyNavBar";
import HomePage from "./Components/HomePage";
import AllProducts from "./Components/Pages/AllProducts";
import AllUsers from "./Components/Pages/AllUsers";
import ProductCard from './Components/ProductCard';
function App() {
  return (
    <div>
      <BigRouter >
         <Routes>
        <Route path="/" index element={<HomePage />} />
        <Route path="/users" element={<AllUsers />} />
        <Route path="/products" element={<AllProducts />} />
      </Routes>
      </BigRouter>
       <MyNavbar />
     
    </div>
  );
}

export default App;
